export const environment = {
  production: true,
  staging: false,
  development: false,
  application: 'test-fdj-angular',
  name: 'Test FDJ Angular',
  api: {
    url: 'https://www.thesportsdb.com/api/v1/json',
    key: 1,
  },
};
