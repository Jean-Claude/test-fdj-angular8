import { Component, OnInit } from '@angular/core';

// services
import { LeaguesService } from '../../../fdj-lib/services/leagues.service';

// models
import { League } from '../../../fdj-lib/models/league.model';

// tools
import { UtilsTool } from '../../../fdj-lib/tools/utils.tool';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  leagues: League[] = [];

  constructor(private leaguesS: LeaguesService, public utilsT: UtilsTool) {}

  ngOnInit() {
    this.leaguesS.getLeagues().then((leagues: League[]) => {
      this.leagues = leagues || [];
    });
  }
}
