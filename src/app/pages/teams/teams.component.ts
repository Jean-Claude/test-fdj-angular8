import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// services
import { TeamsService } from '../../../fdj-lib/services/teams.service';

// models
import { Team } from '../../../fdj-lib/models/team.model';

// tools
import { UtilsTool } from '../../../fdj-lib/tools/utils.tool';

@Component({
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class TeamsComponent implements OnInit {
  teams: Team[] = [];

  constructor(private teamsS: TeamsService, public utilsT: UtilsTool, private activatedR: ActivatedRoute) {}

  ngOnInit() {
    this.activatedR.params.subscribe(e => {
      this.teamsS.getTeamsByLeagueId(this.activatedR.snapshot.params.leagueId).then((teams: Team[]) => {
        this.teams = teams;
      });
    });
  }
}
