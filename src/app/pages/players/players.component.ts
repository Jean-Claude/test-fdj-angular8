import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// services
import { PlayersService } from '../../../fdj-lib/services/players.service';

// models
import { Player } from '../../../fdj-lib/models/player.model';

// tools
import { UtilsTool } from '../../../fdj-lib/tools/utils.tool';

@Component({
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss'],
})
export class PlayersComponent implements OnInit {
  players: Player[] = [];

  constructor(private playersS: PlayersService, public utilsT: UtilsTool, private activatedR: ActivatedRoute) {}

  ngOnInit() {
    this.activatedR.params.subscribe(e => {
      this.playersS.getPlayersByTeamId(this.activatedR.snapshot.params.teamId).then((players: Player[]) => {
        this.players = players;
      });
    });
  }
}
