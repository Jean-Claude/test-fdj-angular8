import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';

import { AppComponent } from './app/app.component';
import { AppRoutingModule } from './app-routing.module';

// fdj-lib
import { SharedModule } from '../fdj-lib/modules/shared.module';

// pages
import { HomeComponent } from './pages/home/home.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { PlayersComponent } from './pages/players/players.component';

// components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LeaguesComponent } from './components/leagues/leagues.component';
import { TeamComponent } from './components/team/team.component';
import { PlayerComponent } from './components/player/player.component';

// services

// tools
import { StorageTool } from './tools/storage.tool';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    // pages
    HomeComponent,
    TeamsComponent,
    PlayersComponent,
    // components
    HeaderComponent,
    FooterComponent,
    LeaguesComponent,
    TeamComponent,
    PlayerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SharedModule.forRoot({
      providers: [StorageTool],
    }),
  ],
  providers: [StorageTool, { provide: LOCALE_ID, useValue: 'fr' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
