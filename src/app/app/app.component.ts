import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { StorageTool } from '../tools/storage.tool';

declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private translateS: TranslateService, private storageT: StorageTool) {}

  ngOnInit() {
    this.initLanguages();
    // this.initScripts();
  }

  initLanguages() {
    this.translateS.addLangs(['en', 'fr']);
    this.translateS.setDefaultLang('en');
    this.storageT.getStorageItem('lang').then(lang => {
      if (!lang) {
        lang = this.translateS.getBrowserLang();
      }
      lang = lang.match(/en|fr/) ? lang : 'en';
      this.translateS.use(lang);
      this.storageT.setStorageItem('lang', lang);
    });
  }

  initScripts() {
    const script = `<script></script>`;
    jQuery('body').append(script);
  }
}
