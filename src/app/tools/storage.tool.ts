import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

import { IStorageTool } from '../../fdj-lib/interfaces/i.storage.tool';

@Injectable()
export class StorageTool implements IStorageTool {
  constructor() {}

  setStorageItem(key: string, value: string): void {
    try {
      localStorage.setItem(`${environment.application}-${key}`, value);
    } catch (e) {}
  }

  async getStorageItem(key: string): Promise<string> {
    try {
      return localStorage.getItem(`${environment.application}-${key}`);
    } catch (e) {}
    return null;
  }

  deleteStorageItem(key: string): void {
    try {
      localStorage.removeItem(`${environment.application}-${key}`);
    } catch (e) {}
  }
}
