import { Component, Input } from '@angular/core';

// models
import { Team } from '../../../fdj-lib/models/team.model';

// models
import { UtilsTool } from '../../../fdj-lib/tools/utils.tool';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
})
export class TeamComponent {
  @Input('team') team: Team;

  constructor(public utilsT: UtilsTool) {}
}
