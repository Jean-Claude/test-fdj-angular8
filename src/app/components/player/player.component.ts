import { Component, Input } from '@angular/core';

// models
import { Player } from '../../../fdj-lib/models/player.model';

// models
import { UtilsTool } from '../../../fdj-lib/tools/utils.tool';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent {
  @Input('player') player: Player;

  constructor(public utilsT: UtilsTool) {}
}
