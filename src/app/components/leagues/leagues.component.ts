import { Component, Input, OnInit } from '@angular/core';

// models
import { League } from '../../../fdj-lib/models/league.model';

// models
import { UtilsTool } from '../../../fdj-lib/tools/utils.tool';

@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.scss'],
})
export class LeaguesComponent implements OnInit {
  @Input('leagues') leagues: League[] = [];

  filteredLeagues: League[] = [];

  public search: string = null;

  constructor(public utilsT: UtilsTool) {}

  ngOnInit() {
    this.filteredLeagues = [...this.leagues];
  }

  onSearch(search) {
    this.filteredLeagues = this.leagues.filter(league => league.strLeague.toLowerCase().includes(search.toLowerCase()));
  }
}
