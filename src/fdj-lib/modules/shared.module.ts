import { NgModule, Provider, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './fdj-lib/assets/i18n/', '.json');
}

// components
import { BoxComponent } from '../components/box/box.component';
import { LoadingComponent } from '../components/loading/loading.component';

// services
import { BaseService } from '../services/base.service';
import { LeaguesService } from '../services/leagues.service';
import { TeamsService } from '../services/teams.service';
import { PlayersService } from '../services/players.service';

// tools
import { UtilsTool } from '../tools/utils.tool';
import { DateTool } from '../tools/date.tool';
import { TranslateTool } from '../tools/translate.tool';

// pipes
import { UcFirstPipe } from '../pipes/ucFirst.pipe';
import { KeysPipe } from '../pipes/keys.pipe';
import { ValuesPipe } from '../pipes/values.pipe';
import { EntriesPipe } from '../pipes/entries.pipe';

const components: any[] = [BoxComponent, LoadingComponent];

const pipes: any[] = [UcFirstPipe, KeysPipe, ValuesPipe, EntriesPipe];

const sharedModule = {
  declarations: [...components, ...pipes],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [...components, ...pipes, TranslateModule],
  providers: [
    // services
    BaseService,
    LeaguesService,
    TeamsService,
    PlayersService,
    // tools
    UtilsTool,
    DateTool,
    TranslateTool,
    {
      provide: LOCALE_ID,
      useFactory: (translateS: TranslateService) => {
        return translateS.currentLang ? translateS.currentLang : 'en';
      },
      deps: [TranslateService],
    },
    ...pipes,
  ],
};

@NgModule(sharedModule)
export class SharedModule {
  static forRoot(includes: { providers: Provider[] } = { providers: [] }) {
    return {
      ngModule: SharedModule,
      declarations: sharedModule.declarations,
      imports: sharedModule.imports,
      exports: sharedModule.exports,
      providers: [...includes.providers, sharedModule.providers],
    };
  }
}
