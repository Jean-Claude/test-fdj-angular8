import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'lib-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss'],
})
export class BoxComponent implements OnChanges {
  @Input() radius: 'small' | 'medium' | 'big' = 'small';
  @Input() color: 'gold' | 'grey' | 'light' | 'dark' = 'grey';
  @Input() border: 'gold' | 'grey' | 'light' | 'dark' = null;
  @Input() breakTopLeft = false;
  @Input() breakTopRight = false;
  @Input() breakBottomLeft = false;
  @Input() breakBottomRight = false;

  ngClass: any = {};

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    this.updateClass();
  }

  updateClass() {
    this.ngClass = {};
    if (this.radius) {
      this.ngClass[`box-radius-${this.radius}`] = true;
    }
    if (this.color) {
      this.ngClass[`box-${this.color}`] = true;
    }
    if (this.border) {
      this.ngClass[`box-border-${this.border}`] = true;
    }
    if (this.breakTopLeft) {
      this.ngClass[`box-break-top-left`] = true;
    }
    if (this.breakTopRight) {
      this.ngClass[`box-break-top-right`] = true;
    }
    if (this.breakBottomLeft) {
      this.ngClass[`box-break-bottom-left`] = true;
    }
    if (this.breakBottomRight) {
      this.ngClass[`box-break-bottom-right`] = true;
    }
  }
}
