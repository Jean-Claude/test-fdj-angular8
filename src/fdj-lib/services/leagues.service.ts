import { Injectable } from '@angular/core';

import { BaseService } from '../../fdj-lib/services/base.service';
import { UtilsTool } from '../../fdj-lib/tools/utils.tool';

import { League } from '../../fdj-lib/models/league.model';

@Injectable()
export class LeaguesService {
  private leagues: League[] = [];

  constructor(private baseS: BaseService, private utilsT: UtilsTool) {}

  async getLeagues() {
    if (this.leagues && this.leagues.length) {
      return this.leagues;
    }
    return this.baseS
      .sendGet('search_all_leagues.php?s=Soccer')
      .then(response => {
        this.baseS.showMessageFromResponse(response);
        const data = this.baseS.getDataJsonFromResponse(response);
        this.leagues = data && data.countrys && data.countrys.length ? data.countrys.map(c => new League(c)) : [];
        return this.leagues;
      })
      .catch(response => {
        this.baseS.showMessageFromResponse(response);
        return [];
      });
  }
}
