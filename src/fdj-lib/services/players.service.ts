import { Injectable } from '@angular/core';

import { BaseService } from '../../fdj-lib/services/base.service';
import { UtilsTool } from '../../fdj-lib/tools/utils.tool';

import { Player } from '../../fdj-lib/models/player.model';

@Injectable()
export class PlayersService {
  constructor(private baseS: BaseService, private utilsT: UtilsTool) {}

  getPlayersByTeamId(teamId = null) {
    return this.baseS
      .sendGet(`lookup_all_players.php?id=${teamId}&s=Soccer`)
      .then(response => {
        this.baseS.showMessageFromResponse(response);
        const data = this.baseS.getDataJsonFromResponse(response);
        return data && data.player && data.player.length ? data.player.map(p => new Player(p)) : [];
      })
      .catch(response => {
        this.baseS.showMessageFromResponse(response);
        return null;
      });
  }
}
