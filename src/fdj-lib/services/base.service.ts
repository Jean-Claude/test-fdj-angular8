import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { UtilsTool } from '../tools/utils.tool';
import { StorageTool } from '../../app/tools/storage.tool';

import { environment } from '../../environments/environment';

@Injectable()
export class BaseService {
  apiUrl = `${environment.api.url}/${environment.api.key}`;

  headers = null;

  constructor(private http: HttpClient, private router: Router, private utilsT: UtilsTool, private storageT: StorageTool) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    this.resetAuthorization();
  }

  sendGet(path, options?) {
    return this.http.get(`${this.apiUrl}/${path}`, options).toPromise();
  }

  sendPost(path, data, options?) {
    const json = this.getDataJson(data);
    return this.http.post(`${this.apiUrl}/${path}`, json, options).toPromise();
  }

  sendPut(path, data, options?) {
    const json = this.getDataJson(data);
    return this.http.put(`${this.apiUrl}/${path}`, json, options).toPromise();
  }

  sendDelete(path, options?) {
    return this.http.delete(`${this.apiUrl}/${path}`, options).toPromise();
  }

  sendGetApi(url) {
    return this.http.get(url, { headers: this.headers, withCredentials: true }).toPromise();
  }

  sendPostApi(url, data) {
    const json = this.getDataJson(data);
    return this.http.post(url, json, { headers: this.headers, withCredentials: true }).toPromise();
  }

  sendPutApi(url, data) {
    const json = this.getDataJson(data);
    return this.http.put(url, json, { headers: this.headers, withCredentials: true }).toPromise();
  }

  sendDeleteApi(url) {
    return this.http.delete(url, { headers: this.headers, withCredentials: true }).toPromise();
  }

  setLanguage(lang = 'en') {
    this.headers.set('Accept-Language', lang);
  }

  resetAuthorization() {
    this.storageT.getStorageItem('token').then(token => {
      this.headers.delete('Authorization');
      if (token) {
        this.headers.append('Authorization', token);
      }
    });
  }

  setTokenFromResponse(res) {
    if (res) {
      const json = res.json();
      if (json && json.token) {
        this.storageT.setStorageItem('token', json.token);
        this.headers.append('Authorization', json.token);
        return json.token;
      }
    }
    return null;
  }

  redirect403(res) {
    if (res.status === 403) {
      this.router.navigate(['/']);
    }
  }

  showMessageFromResponse(res) {
    if (res) {
      if (res.message) {
        if (res.status >= 400) {
          this.utilsT.showMessageError(res.message);
        } else {
          this.utilsT.showMessageSuccess(res.message);
        }
      }
    }
  }

  getDataClean(obj) {
    for (const key in obj) {
      if (obj[key]) {
        if (this.utilsT.isObject(obj[key])) {
          this.getDataClean(obj[key]);
        } else if (obj[key] === 'null') {
          obj[key] = null;
        }
      }
    }
    return obj;
  }

  getDataJson(obj, log = false) {
    const data = this.getDataClean(this.utilsT.copyObject(obj));
    if (log) {
      console.log('DATA', data);
    }
    return this.utilsT.jsonEncode(data);
  }

  getDataJsonFromResponse(res) {
    if (res) {
      return res;
    }
    return null;
  }

  getUserFromResponse(res) {
    if (res) {
      const json = res.json();
      if (json && json._user) {
        return json._user;
      }
    }
    return {};
  }

  listing(action, subAction = 'listing', log = true) {
    action += '/' + subAction;
    return this.sendGetApi(action)
      .then(res => {
        return this.getDataJsonFromResponse(res);
      })
      .catch(res => {
        if (log) {
          this.showMessageFromResponse(res);
          this.redirect403(res);
        }
        return null;
      });
  }

  view(action, id, subAction = 'view') {
    return this.sendGetApi(action + '/' + subAction + '/' + id)
      .then(res => {
        return this.getDataJsonFromResponse(res);
      })
      .catch(res => {
        this.showMessageFromResponse(res);
        this.redirect403(res);
        return null;
      });
  }

  add(action, elem, subAction = 'add') {
    return this.sendPostApi(action + '/' + subAction, elem)
      .then(res => {
        this.showMessageFromResponse(res);
        return this.getDataJsonFromResponse(res);
      })
      .catch(res => {
        this.redirect403(res);
        this.showMessageFromResponse(res);
        return null;
      });
  }

  edit(action, id, elem, subAction = 'edit') {
    return this.sendPostApi(action + '/' + subAction + '/' + id, elem)
      .then(res => {
        this.showMessageFromResponse(res);
        return this.getDataJsonFromResponse(res);
      })
      .catch(res => {
        this.redirect403(res);
        this.showMessageFromResponse(res);
        return null;
      });
  }

  delete(action, id, subAction = 'delete') {
    return this.sendDeleteApi(action + '/' + subAction + '/' + id)
      .then(res => {
        this.showMessageFromResponse(res);
        return true;
      })
      .catch(res => {
        this.redirect403(res);
        this.showMessageFromResponse(res);
        return false;
      });
  }
}
