import { Injectable } from '@angular/core';

import { BaseService } from '../../fdj-lib/services/base.service';
import { UtilsTool } from '../../fdj-lib/tools/utils.tool';

import { Team } from '../../fdj-lib/models/team.model';

@Injectable()
export class TeamsService {
  constructor(private baseS: BaseService, private utilsT: UtilsTool) {}

  getTeamsByLeagueId(leagueId) {
    return this.baseS
      .sendGet(`lookup_all_teams.php?id=${leagueId}&s=Soccer`)
      .then(response => {
        this.baseS.showMessageFromResponse(response);
        const data = this.baseS.getDataJsonFromResponse(response);
        return data && data.teams && data.teams.length ? data.teams.map(t => new Team(t)) : [];
      })
      .catch(response => {
        this.baseS.showMessageFromResponse(response);
        return null;
      });
  }
}
