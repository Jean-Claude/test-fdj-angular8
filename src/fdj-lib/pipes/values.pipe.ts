import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'values' })
export class ValuesPipe implements PipeTransform {
  transform(object): Object {
    if (object && typeof object === 'object') {
      return Object.values(object);
    }
    return object;
  }
}
