import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'entries' })
export class EntriesPipe implements PipeTransform {
  transform(object): Object {
    if (object && typeof object === 'object') {
      return Object.entries(object);
    }
    return object;
  }
}
