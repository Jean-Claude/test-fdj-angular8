import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {
  transform(object): Object {
    if (object && typeof object === 'object') {
      return Object.keys(object);
    }
    return object;
  }
}
