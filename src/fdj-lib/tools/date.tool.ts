import { Injectable } from '@angular/core';

import * as moment from 'moment/moment';

@Injectable()
export class DateTool {
  constructor() {}

  getYYYYMMDD(date?: any) {
    return moment(date).format('YYYY-MM-DD');
  }

  getYYYYMMDDHHMMSS(date?: any) {
    return moment(date).format('YYYY-MM-DD HH:mm:ss');
  }

  getDDMMYY(date?: any) {
    return moment(date).format('DD-MM-YYYY');
  }
}
