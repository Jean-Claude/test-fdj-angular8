import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

// tools
import { TranslateTool } from './translate.tool';

@Injectable()
export class UtilsTool {
  constructor(private translateT: TranslateTool, private toastrS: ToastrService, private domSanitizer: DomSanitizer) {}

  getWidth() {
    return document.body.clientWidth;
  }

  showMessageSuccess(message: string, title: string = '') {
    this.toastrS.success(this.translateT.translate(message), title);
  }

  showMessageError(message: string, title: string = '') {
    this.toastrS.error(this.translateT.translate(message), title);
  }

  isObject(obj) {
    return typeof obj === 'object';
  }

  isArray(obj) {
    return Array.isArray(obj);
  }

  isEmptyObject(obj) {
    for (const i in obj) {
      if (obj[i]) {
        if (this.isArray(obj[i])) {
          if (obj[i].length) {
            return false;
          }
        } else if (this.isObject(obj[i])) {
          if (this.isEmptyObject(obj[i]) === false) {
            return false;
          }
        } else if (obj[i]) {
          return false;
        }
      }
    }
    return true;
  }

  isEmptyArrayValue(array) {
    for (let i = 0; i < array.length; i++) {
      if (array[i]) {
        return false;
      }
    }
    return true;
  }

  getById(id, objs) {
    for (const i in objs) {
      if (objs[i] && objs[i]['id'] === id) {
        return objs[i];
      }
    }
    return null;
  }

  deleteById(id, objs) {
    for (const i in objs) {
      if (objs[i] && objs[i]['id'] === id) {
        objs.splice(i, 1);
        return true;
      }
    }
    return false;
  }

  getFieldById(id, field, objs) {
    for (const i in objs) {
      if (objs[i] && objs[i]['id'] === id) {
        return objs[i][field] || null;
      }
    }
    return null;
  }

  copyObject(obj: any) {
    return JSON.parse(JSON.stringify(obj));
  }

  mergeObject(dest: any, src: any) {
    for (const i in src) {
      if (src[i] && this.isArray(dest[i]) && this.isArray(src[i])) {
        dest[i].length = 0;
        for (const j in src[i]) {
          if (src[i][j]) {
            dest[i].push(src[i][j]);
          }
        }
      } else {
        dest[i] = src[i];
      }
    }
  }

  getDuplicateObjectInArrayByField(arr, field) {
    const obj = {};
    for (let i = 0; i < arr.length; i++) {
      const value = arr[i][field];
      if (value) {
        if (obj[value]) {
          return arr[i];
        }
        obj[value] = arr[i];
      }
    }
    return null;
  }

  ucFirst(str: string) {
    if (str && typeof str === 'string') {
      return str.charAt(0).toUpperCase() + str.slice(1);
    }
    return str;
  }

  ucWords(str: string) {
    return (str + '').replace(/^(.)|\s+(.)/g, val => {
      return val.toUpperCase();
    });
  }

  roundNumber(nb: number): number {
    return Math.round(nb * 100) / 100;
  }

  keepTwoDigitNumber(nb: number): number {
    return Math.floor(nb * 100) / 100;
  }

  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  padWithZeroes(number, length) {
    let str = '' + number;
    while (str.length < length) {
      str = '0' + str;
    }
    return str;
  }

  jsonEncode(json) {
    try {
      return JSON.stringify(json);
    } catch (e) {}
    return null;
  }

  jsonDecode(str) {
    try {
      return JSON.parse(str);
    } catch (e) {}
    return null;
  }

  goToTopPage() {
    window.scrollTo(0, 0);
  }

  deg2rad(value) {
    return (value * Math.PI) / 180;
  }

  rad2deg(value) {
    return (value / Math.PI) * 180;
  }

  removeHtmlTags(str: string) {
    return str.replace(/<\/?[^>]+(>|$)/g, '');
  }

  spliceWithoutHtml(str: string, start, end, dots = true) {
    str = this.removeHtmlTags(str);
    if (str) {
      if (str.length > start + end) {
        return str.slice(start, end) + (dots ? '...' : '');
      }
    }
    return str;
  }

  dataURItoText(dataURI) {
    const array = dataURI.split(',');
    if (array.length === 2) {
      return atob(array[1]);
    }
    return null;
  }

  dataURItoBlob(dataURI) {
    const byteString = atob(dataURI.split(',')[1]);
    const mimeString = dataURI
      .split(',')[0]
      .split(':')[1]
      .split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([ab], { type: mimeString });
    return blob;
  }

  readFileContent(e, pattern = null) {
    return new Promise((resolve, reject) => {
      const input = e.dataTransfer ? e.dataTransfer : e.target;
      const file = input.files[0];
      const reader = new FileReader();
      if (pattern && !file.type.match(pattern)) {
        reject('invalid format');
      } else {
        reader.onload = event => {
          resolve({
            file: file,
            event: event,
            reader: reader,
          });
        };
        reader.readAsDataURL(file);
      }
      input.value = '';
    });
  }

  urlSanitizer(url) {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
