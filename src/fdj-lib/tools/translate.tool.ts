import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class TranslateTool {
  constructor(private translateS: TranslateService) {}

  private _ucFirst(str: string) {
    if (str && typeof str === 'string') {
      return str.charAt(0).toUpperCase() + str.slice(1);
    }
    return str;
  }

  getCurrentLang() {
    return this.translateS.currentLang;
  }

  translate(str: string) {
    return this.translateS.instant(str);
  }

  translateUcFirst(str: string) {
    return this._ucFirst(this.translate(str));
  }
}
