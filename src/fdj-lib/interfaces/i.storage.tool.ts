export interface IStorageTool {
  setStorageItem(key: string, value: string): void;
  getStorageItem(key: string): Promise<string>;
  deleteStorageItem(key: string): void;
}
