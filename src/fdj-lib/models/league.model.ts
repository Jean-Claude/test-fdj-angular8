// models
import { Base } from './base.model';
import { Team } from './team.model';

export class League extends Base {
  idLeague: number = null;
  strLeague: string = null;
  strLeagueBadge: string = null;
  teams: Team[] = [];

  constructor(obj = null) {
    super(obj);
    const fields = ['idLeague', 'strLeague', 'strLeagueBadge'];
    this.init(obj, fields);
    this.addSaveFields([...fields, 'teams']);
    this.initArray(obj.teams, this.teams, Team);
  }

  objectToSave() {
    const obj: any = super.objectToSave();
    return obj;
  }
}
