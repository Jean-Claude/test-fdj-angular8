// models
import { Base } from './base.model';
import { Player } from './player.model';

export class Team extends Base {
  idTeam: number = null;
  strTeam: string = null;
  strTeamBadge: string = null;
  players: Player[] = [];

  constructor(obj = null) {
    super(obj);
    const fields = ['idTeam', 'strTeam', 'strTeamBadge'];
    this.init(obj, fields);
    this.addSaveFields([...fields, 'players']);
    this.initArray(obj.players, this.players, Player);
  }

  objectToSave() {
    const obj: any = super.objectToSave();
    return obj;
  }
}
