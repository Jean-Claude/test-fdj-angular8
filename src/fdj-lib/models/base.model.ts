import * as moment from 'moment';
import * as _ from 'lodash';

export class Base {
  saveFields = [];
  hasCreatedAt = true;
  hasUpdatedAt = true;

  // fields
  key?: string = null;
  createdAt?: string = null;
  updatedAt?: string = null;

  constructor(obj = null) {
    if (obj) {
      this.key = obj.key || null;
      if (obj.createdAt) {
        this.createdAt = this.isoDate(obj.createdAt);
      }
      if (obj.updatedAt) {
        this.updatedAt = this.isoDate(obj.updatedAt);
      }
    }
  }

  isoDate(date = moment()) {
    let ret = date;
    if (!moment.isMoment(date)) {
      ret = moment(date);
    }
    return ret.toISOString();
  }

  getYYYYMMDD(date?) {
    return moment(date).format('YYYY-MM-DD');
  }

  addSaveFields(fields: Array<string>) {
    this.saveFields = this.saveFields.concat(fields);
  }

  private _objectsToSave(dest: Array<any> | object, src: Array<any> | object) {
    for (const i in src) {
      if (src.hasOwnProperty(i)) {
        const elem = src[i];
        if (Array.isArray(elem)) {
          dest[i] = [];
          this._objectsToSave(dest[i], elem);
        } else if (elem instanceof Base) {
          dest[i] = elem.objectToSave();
        } else if (elem instanceof Date) {
          dest[i] = this.getYYYYMMDD(elem);
        } else if (typeof elem === 'object') {
          dest[i] = {};
          this._objectsToSave(dest[i], elem);
        } else {
          dest[i] = elem;
        }
      }
    }
  }

  objectToSave() {
    const obj: any = {};
    if (this.hasCreatedAt) {
      obj.createdAt = this.createdAt || this.isoDate();
    }
    if (this.hasUpdatedAt) {
      obj.updatedAt = this.isoDate();
    }
    for (const field of this.saveFields) {
      const val = this[field];
      if (Array.isArray(val)) {
        obj[field] = [];
        this._objectsToSave(obj[field], val);
      } else if (val instanceof Date) {
        obj[field] = this.getYYYYMMDD(val);
      } else if (val instanceof Base) {
        obj[field] = val.objectToSave();
      } else if (val && typeof val === 'object') {
        obj[field] = {};
        this._objectsToSave(obj[field], val);
      } else {
        obj[field] = val;
      }
    }
    return obj;
  }

  init(obj = null, fields = []) {
    if (obj) {
      if (obj.key) {
        this.key = obj.key;
      }
      for (const f of fields) {
        const val = obj[f];
        if (val !== null && val !== undefined) {
          this[f] = this._duplicate(val);
        }
      }
    }
  }

  _duplicate(val) {
    if (Array.isArray(val)) {
      const array = [];
      for (const e of val) {
        array.push(this._duplicate(e));
      }
      return array;
    } else if (typeof val === 'object') {
      return _.cloneDeep(val);
    }
    return val;
  }

  initArray(src, dest, Type) {
    if (src) {
      for (const elem of src) {
        dest.push(new Type(elem));
      }
    }
  }
}
