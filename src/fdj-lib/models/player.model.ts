// models
import { Base } from './base.model';

export class Player extends Base {
  idPlayer: number = null;
  strPlayer: string = null;
  strThumb: string = null;
  strCutout: string = null;
  dateBorn: string = null;
  strPosition: string = null;
  strSigning: string = null;

  constructor(obj = null) {
    super(obj);
    const fields = ['idPlayer', 'strPlayer', 'strThumb', 'strCutout', 'dateBorn', 'strPosition', 'strSigning'];
    this.init(obj, fields);
    this.addSaveFields(fields);
  }
}
